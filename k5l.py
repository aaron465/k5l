import click
import kubernetes
from tabulate import tabulate


def k8s_core_v1(context=None):
    'Return the corev1 api'
    kubernetes.config.load_kube_config(context=context)
    return kubernetes.client.CoreV1Api()


@click.group()
def cli_entrypoint():
    pass


@click.group()
def get():
    'Get a thing'


@click.command('nodes')
@click.option('--context', help='kubectl context')
def get_nodes(context=None):
    'Get nodes'
    nodes = k8s_core_v1(context=context).list_node().items
    click.echo(tabulate(
        [[
            node.metadata.name,
            node.metadata.labels['failure-domain.beta.kubernetes.io/zone'],
            node.metadata.labels['beta.kubernetes.io/instance-type'],
        ] for node in nodes],
        headers=['NAME', 'ZONE', 'INSTANCE-TYPE'],
    ))


get.add_command(get_nodes)
cli_entrypoint.add_command(get)


if __name__ == '__main__':
    cli_entrypoint()

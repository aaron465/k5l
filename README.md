# k5l

Like "Kubernetes" is shortened to "k8s", "kubectl" is shortened to "k5l".

This is an alternate Kubernetes command line client specifically to make my life easier for a few
specific tasks. It's quite unlikely that anybody else is going to need/want this, but feel free
to raise a merge request or do whatever you like with this code!

-Aaron
